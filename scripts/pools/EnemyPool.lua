local enemyPool = {}

local apiUtils = require("SynchronyExtendedAPI.utils.APIUtils")

local ecs = require("system.game.Entities")
local utils = require("system.utils.Utilities")

local rng = require("necro.game.system.RNG")
local event = require("necro.event.Event")
local object = require("necro.game.object.Object")

local substitutions = {}
local substitutionCounter = 0

function enemyPool.addReplacement(enemyName, replacementName, weight, overwriteBaseType)
    substitutionCounter = substitutionCounter + 1
    apiUtils.safeAddEvent(
        event.gameStateLevel,
        "addEnemySubstitution" .. substitutionCounter,
        {
            order = "level",
            sequence = 0,
        },
        function(ev)
            if substitutions[enemyName] == nil then
                substitutions[enemyName] = {
                    originalType = { weight = overwriteBaseType and 0 or 50, name = enemyName, },
                }
            else
                substitutions[enemyName].originalType.weight = 0
            end
            substitutions[enemyName][replacementName] = { weight = weight, name = replacementName, }
        end)
end

local function convertEntity(indexedSubstitutions, weightSum, baseType)
    local roll = rng.int(weightSum, baseType)
    for _, rangeType in ipairs(indexedSubstitutions) do
        if roll >= rangeType.rangeMin and roll < rangeType.rangeMax then
            local replacementEntitiy = ecs.resolveEntityType(rangeType.type.name)
            if replacementEntitiy ~= ecs.getEntityTypeID(baseType) then
                object.spawn(replacementEntitiy, baseType.position.x, baseType.position.y)
                return true
            else
                return false
            end
        end
    end
    return false
end

apiUtils.safeAddEvent(
    event.gameStateLevel,
    "clearEnemySubstitutions",
    {
        order = "level",
        sequence = -1,
    },
    function(ev)
        -- reset substitutions
        substitutions = {}
    end)


apiUtils.safeAddEvent(
    event.gameStateLevel,
    "transformEnemies",
    {
        order = "level",
        sequence = 1,
    },
    function(ev)
        local index = 1
        for _, substitution in utils.sortedPairs(substitutions) do
            local indexedSubstitutions = {}
            local despawnList = {}

            local weightSum = 0
            for _, type in utils.sortedPairs(substitution) do
                table.insert(indexedSubstitutions, { type = type, rangeMin = weightSum, rangeMax = weightSum + type.weight })
                weightSum = weightSum + type.weight
            end

            local entity = ecs.getEntitiesByType(substitution.originalType.name)
            while entity:next() do
                if convertEntity(indexedSubstitutions, weightSum, entity) then
                    table.insert(despawnList, entity.id)
                end
            end

            for _, enemyID in ipairs(despawnList) do
                object.delete(ecs.getEntityByID(enemyID))
            end
            index = index + 1
        end
    end)

return enemyPool

