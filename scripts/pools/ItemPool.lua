local itemPool = {}

local entities = require("SynchronyExtendedAPI.extended.Entities")
local apiUtils = require("SynchronyExtendedAPI.utils.APIUtils")

local ecs = require("system.game.Entities")
local utils = require("system.utils.Utilities")

local rng = require("necro.game.system.RNG")
local event = require("necro.event.Event")
local object = require("necro.game.object.Object")

local priceTag = require("necro.game.item.PriceTag")
local inventory = require("necro.game.item.Inventory")

local components = require("necro.game.data.Components")
local field = components.field
local constant = components.constant

components.register {
    itemSubstitution = {
        field.bool("isFinal", false)
    },
}

entities.modifyAllItems(
    "addItemReplacementFlag",
    function(ev)
        ev.entity.SynchronyExtendedAPI_itemSubstitution = {}
    end)

local substitutions = {{}, {}, {}}
local substitutionCounter = 0

local itemPoolType = {
    FLOOR = 1,
    HOLDER = 2,
    SHOP = 3,
}

local function addReplacement(type, itemName, replacementName, price, holderName, shopkeeperName, weight, overwriteBaseType)
    substitutionCounter = substitutionCounter + 1
    apiUtils.safeAddEvent(
        event.gameStateLevel,
        "addItemSubstitution" .. substitutionCounter,
        {
            order = "priceTags",
            sequence = 0,
        },
        function()
            if substitutions[type][itemName] == nil then
                substitutions[type][itemName] = {
                    originalType = {
                        name = itemName,
                        weight = overwriteBaseType and 0 or 50,
                    },
                }
            elseif overwriteBaseType then
                substitutions[type][itemName].originalType.weight = 0
            end
            substitutions[type][itemName][replacementName] = {
                name = replacementName,
                price = price,
                holder = holderName,
                shopkeeper = shopkeeperName,
                weight = weight,
            }
        end)
end

function itemPool.addShopReplacement(shopkeeperName, itemName, replacementName, price, weight, overwriteBaseType)
    addReplacement(itemPoolType.SHOP, itemName, replacementName, price, false, shopkeeperName, weight, overwriteBaseType)
end

function itemPool.addHolderReplacement(holderName, itemName, replacementName, weight, overwriteBaseType)
    addReplacement(itemPoolType.HOLDER, itemName, replacementName, 0, holderName, false, weight, overwriteBaseType)
end

function itemPool.addFloorReplacement(itemName, replacementName, weight, overwriteBaseType)
    addReplacement(itemPoolType.FLOOR, itemName, replacementName, 0, false, false, weight, overwriteBaseType)
end

local function convertEntity(indexedSubstitutions, weightSum, baseType, paymentType)
    local roll = rng.int(weightSum, baseType)
    for _, rangeType in ipairs(indexedSubstitutions) do
        if roll >= rangeType.rangeMin and roll < rangeType.rangeMax then
            local replacementEntitiy = ecs.resolveEntityType(rangeType.type.name)
            if replacementEntitiy ~= ecs.getEntityTypeID(baseType) then
                local newEntity = object.spawn(replacementEntitiy, baseType.position.x, baseType.position.y)
                newEntity.SynchronyExtendedAPI_itemSubstitution.isFinal = true

                if paymentType then
                    local priceTagEntity = ecs.getEntityByID(baseType.sale.priceTag)
                    priceTagEntity[paymentType].cost = rangeType.price
                    priceTag.add(newEntity, priceTagEntity)
                end

                if baseType.item.holder > 0 then
                    inventory.add(newEntity, ecs.getEntityByID(baseType.item.holder), true)
                end

                return true
            else
                return false
            end
        end
    end
    return false
end

apiUtils.safeAddEvent(
    event.gameStateLevel,
    "clearItemSubstitutions",
    {
        order = "priceTags",
        sequence = -1,
    },
    function(ev)
        -- reset substitutions
        substitutions = {{}, {}, {}}
    end)

local function performSubstitution(items, poolType)
    local hasHolder = poolType == itemPoolType.HOLDER
    local hasShopkeeper = poolType == itemPoolType.SHOP
    
    for _, substitution in utils.sortedPairs(substitutions[poolType]) do
        local indexedSubstitutions = {}
        local despawnList = {}

        local weightSum = 0
        for _, itemType in utils.sortedPairs(substitution) do
            local correctHolder = (hasHolder and itemType.holder) or (not hasHolder and not itemType.holder)
            local correctShopkeeper = (hasShopkeeper and itemType.shopkeeper) or (not hasShopkeeper and not itemType.shopkeeper)
            if correctHolder and correctShopkeeper then
                table.insert(indexedSubstitutions, {
                    type = itemType,
                    price = hasShopkeeper and itemType.price or 0,
                    rangeMin = weightSum,
                    rangeMax = weightSum + itemType.weight
                })
                weightSum = weightSum + itemType.weight
            end
        end

        for _, item in ipairs(items) do
            local correctHolder = (hasHolder and item.holder) or (not hasHolder and not item.holder)
            local correctShopkeeper = (hasShopkeeper and item.shopkeeper) or (not hasShopkeeper and not item.shopkeeper)

            if item.itemName == substitution.originalType.name and correctHolder and correctShopkeeper then
                local entity = ecs.getEntityByID(item.itemID)
                if convertEntity(indexedSubstitutions, weightSum, entity, item.paymentType) then
                    table.insert(despawnList, entity.id)
                end
                entity.SynchronyExtendedAPI_itemSubstitution.isFinal = true
            end
        end
    
        for _, itemID in ipairs(despawnList) do
            object.delete(ecs.getEntityByID(itemID))
        end
    end
end

apiUtils.safeAddEvent(
    event.gameStateLevel,
    "transformItems",
    {
        order = "priceTags",
        sequence = 1,
    },
    function(ev)
        local holderItems = {}
        local shopItems = {}
        local floorItems = {}

        -- prepare items
        for _, entity in ecs.liveTypesWithComponents({ "item", "SynchronyExtendedAPI_itemSubstitution" }) do
            while entity:next() do
                if not entity.SynchronyExtendedAPI_itemSubstitution.isFinal then
                    if entity.item.holder ~= nil and entity.item.holder ~= 0 then
                        if ecs.entityExists(entity.item.holder) then
                            local holderType = ecs.getEntityTypeName(ecs.getEntityByID(entity.item.holder))
                            table.insert(holderItems, {
                                itemName = ecs.getEntityTypeName(entity),
                                itemID = entity.id,
                                holder = holderType,
                                shopkeeper = false,
                                paymentType = false,
                            })
                        end
                    elseif entity.sale.priceTag ~= nil and entity.sale.priceTag ~= 0 then
                        if ecs.entityExists(entity.sale.priceTag) then
                            local priceTag = ecs.getEntityByID(entity.sale.priceTag)
                            local shopkeeper = ecs.getEntityByID(priceTag.priceTagShopkeeperProximity.shopkeeper)
                            local shopkeeperType = ecs.getEntityTypeName(shopkeeper)

                            local paymentType = "priceTagCostCurrency"
                            if priceTag:hasComponent("priceTagCostHealth") then
                                paymentType = "priceTagCostHealth"
                            end
                            
                            table.insert(shopItems, {
                                itemName = ecs.getEntityTypeName(entity),
                                itemID = entity.id,
                                holder = false,
                                shopkeeper = shopkeeperType,
                                paymentType = paymentType,
                            })
                        end
                    else
                        table.insert(floorItems, {
                            itemName = ecs.getEntityTypeName(entity),
                            itemID = entity.id,
                            holder = false,
                            shopkeeper = false,
                            paymentType = false,
                        })
                    end
                end
            end
        end

        performSubstitution(holderItems, itemPoolType.HOLDER)
        performSubstitution(shopItems, itemPoolType.SHOP)
        performSubstitution(floorItems, itemPoolType.FLOOR)
    end)

return itemPool

