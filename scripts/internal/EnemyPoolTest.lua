local enemyPool = require("SynchronyExtendedAPI.pools.EnemyPool")

-- -- Test valid replacement
-- enemyPool.addReplacement("Skeleton", "Slime", 50, true)
-- enemyPool.addReplacement("Skeleton2", "Slime", 50, true)
-- enemyPool.addReplacement("Skeleton3", "Slime", 50, true)
-- enemyPool.addReplacement("Skeleton4", "Slime", 50, true)
-- enemyPool.addReplacement("Slime", "Slime", 50, true)
-- enemyPool.addReplacement("Slime2", "Slime", 50, true)
-- enemyPool.addReplacement("Slime3", "Slime", 50, true)
-- enemyPool.addReplacement("Slime4", "Slime", 50, true)
-- enemyPool.addReplacement("Bat", "Slime", 50, true)
-- enemyPool.addReplacement("Bat2", "Slime", 50, true)
-- enemyPool.addReplacement("Bat3", "Slime", 50, true)
-- enemyPool.addReplacement("Bat4", "Slime", 50, true)
-- enemyPool.addReplacement("Zombie", "Slime", 50, true)
-- enemyPool.addReplacement("Monkey", "Slime", 50, true)
-- enemyPool.addReplacement("Ghost", "Slime", 50, true)
-- enemyPool.addReplacement("Wraith", "Slime", 50, true)

-- -- Test invalid replacement
-- enemyPool.addReplacement("Slime", "Bork", 50, true)