local apiUtils = {}

local debugConsole = require("SynchronyExtendedAPI.utils.DebugConsole")

--local stacktrace = require("system.debug.StackTrace")
local utils = require("system.utils.Utilities")


function apiUtils.safeCall(callback, parameters)
    local success, result = pcall(callback, parameters)
    if not success then
        debugConsole.printError(utils.inspect(result))
        --local level = (parameters and parameters.level) and parameters.level or 2
        --dbg(stacktrace.traceback(result, level))
    end
end

function apiUtils.safeAddEvent(event, eventName, parameters, callback)
    apiUtils.safeCall(
        function ()
            event.add(eventName, parameters, function(ev) apiUtils.safeCall(callback, ev) end)
        end)
end

function apiUtils.safeOverrideEvent(event, eventName, parameters, callback)
    apiUtils.safeCall(
        function ()
            event.override(eventName, parameters, function(func, ev) apiUtils.safeCall(function() callback(func, ev) end) end)
        end)
end

-- -- only for testing
-- local event = require "necro.event.Event"
-- event.gameStateLevel.add(
--     "testMessage",
--     "level",
--     function(ev)

--         local success, result = pcall(function() error("Super lange nachricht und so") end)

--         if not success then
--             debugConsole.printError(utils.inspect(result))
--             debugConsole.printError("message")
--             debugConsole.printDebug(utils.inspect(ev))
--         end
--     end)

return apiUtils