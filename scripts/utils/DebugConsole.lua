local debugConsole = {}

local theme = require "necro.config.Theme"
local tick = require "necro.cycles.Tick"
local ui = require "necro.render.UI"
local event = require "necro.event.Event"
local singlePlayer = require "necro.client.SinglePlayer"

local color = require "system.utils.Color"
local gfx = require "system.game.Graphics"
local stringUtils = require "system.utils.StringUtilities"
local utils = require "system.utils.Utilities"

local debugMessages = {}

local function addMessage(message)
	log.info("%s%s",
		message.name and (stringUtils.fromUnicode(message.name) .. ": ") or "",
		stringUtils.fromUnicode(message.text))

    local messages = stringUtils.split(message.text, "\n")
    local lines = {}
    local maxChars = 60
    for i, line in ipairs(messages) do
        for i = 1, math.ceil(line:len() / maxChars), 1 do
            table.insert(lines, line:sub((i - 1) * maxChars + 1, math.min(line:len(), i * maxChars)))
        end
    end

    for i, line in ipairs(lines) do
        table.insert(debugMessages, 1, {
            name = message.name,
            nameColor = message.nameColor or theme.getLocalNameColor(),
            text = line,
            displayTime = message.displayTime or tick.seconds(8),
            color = message.color or theme.getLocalChatColor(),
            position = -1
        })
    end
end

function debugConsole.printError(message)
    debugConsole.print(message, color.hsv(0, 1, 0.8))
end

function debugConsole.printDebug(message)
    if singlePlayer.isActive() then
        debugConsole.print(message, color.hsv(0.6, 1, 0.8))
    end
end

function debugConsole.print(message, color)
	if type(message) == "table" then
		message = utils.inspect(message)
	else
		message = tostring(message)
	end
	addMessage {
		text = message,
		color = color or theme.getSystemMessageColor()
	}
end

event.tick.add("handleDebugConsole", "chat", function ()
	for i = 1, #debugMessages do
		local message = debugMessages[i]
		message.displayTime = message.displayTime - 1
		message.position = utils.lerp(message.position, i, 0.125)
	end

    utils.removeIf(debugMessages, function (message)
		return message.displayTime < 0
	end)
end)

local function renderChatMessage(message)
    local maxLines = 20
    if (message.position > maxLines) then
        return
    end

	local textSize = 10
	local spacing = 5
	local nameMessageSpacing = 15
	local x, y = gfx.getWidth() - 400, gfx.getHeight() - 80 - textSize
	local fadeTime = tick.seconds(0.5)
	local fade = math.min(1, (message.displayTime or fadeTime) / fadeTime)
	local textData = {
		font = ui.Font.VECTOR,
		uppercase = false,
		x = x,
		y = y - message.position * (spacing + textSize),
		size = textSize,
		fillColor = color.fade(message.nameColor, fade),
		outlineColor = color.fade(theme.getChatOutlineColor(), fade),
		outlineThickness = 1,
		shadowColor = color.TRANSPARENT,
        gradient = false,
	}
	if message.name ~= nil then
		textData.text = stringUtils.toUnicode(message.name)
		utils.appendToTable(textData.text, stringUtils.toUnicode(":"))
		local shift = ui.drawText(textData).width
		textData.x = textData.x + shift + nameMessageSpacing
	end
	if message.text ~= nil then
		textData.fillColor = color.fade(message.color, fade)
		textData.text = stringUtils.toUnicode(message.text)
    end
    
    ui.drawText(textData)
end

event.render.add("renderDebugConsole", "chat", function ()
    if #debugMessages > 0 then
        gfx.drawBox(
            {
                gfx.getWidth() - 420,
                gfx.getHeight() - 100 - 20 * 15,
                420,
                20 * 15 + 20
            },
            color.rgba(0, 0, 0, 200))
    end

	for i = 1, #debugMessages do
		renderChatMessage(debugMessages[i])
	end
end)

return debugConsole
