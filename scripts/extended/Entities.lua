--- Provides methods for adding and removing compontents of all entities.
local entities = {}

local apiUtils = require("SynchronyExtendedAPI.utils.APIUtils")
local debugConsole = require("SynchronyExtendedAPI.utils.DebugConsole")

local event = require "necro.event.Event"
local customEntities = require "necro.game.data.CustomEntities"

--- Override components of players.
--- @param eventName string | "yourEventName"
--- @param callback function | "function(ev) end"
function entities.modifyAllPlayers(eventName, callback)
    apiUtils.safeAddEvent(
            event.entitySchemaLoadPlayer,
            eventName,
            {
                order = "overrides",
                sequence = 1,
            },
            callback)
end

local function getPlayerId(template)
    local playerId = nil
    if template ~= nil then
        if type(template) == "table" and template.id then
            playerId = template.id
        else
            debugConsole.printError("Invlalid template:")
            debugConsole.printError(template)
        end
    end
    return playerId
end

--- Override components of a player.
--- @param eventName string | "yourEventName"
--- @param template table | "namedPlayers.PlayerName"
--- @param callback function | "function(ev) end"
function entities.modifyPlayer(eventName, template, callback)
    apiUtils.safeAddEvent(
            event.entitySchemaLoadPlayer,
            eventName,
            {
                order = "overrides",
                sequence = 1,
            },
            function(ev)
                if getPlayerId(template) == ev.data.id then
                    callback(ev)
                end
            end)
end

--- Add a new player based on a template character.
--- @param template table|nil | "namedPlayers.PlayerName"
--- @param parameters table | "{\n    name = yourCharacterName\\,\n    data = {}\\,\n    components = {}\\,\n}"
function entities.addPlayer(template, parameters)
    apiUtils.safeCall(
        function()
            parameters.template = customEntities.template.player(getPlayerId(template))
            customEntities.extend(parameters)
        end)
end

--- Override components of items.
--- @param eventName string | "yourEventName"
--- @param callback function | "function(ev) end"
function entities.modifyAllItems(eventName, callback)
    apiUtils.safeAddEvent(
            event.entitySchemaLoadItem,
            eventName,
            {
                order = "slotOverrides",
                sequence = 1,
            },
            callback)
end

local function getItemName(template)
    local itemName = nil
    if template ~= nil then
        if type(template) == "table" and template.xmlName then
            itemName = template.xmlName
        else
            debugConsole.printError("Invlalid template:")
            debugConsole.printError(template)
        end
    end

    return itemName
end

--- Override components of a named item.
--- @param eventName string | "yourEventName"
--- @param template table | "namedItems.ItemName"
--- @param callback function | "function(ev) end"
function entities.modifyItem(eventName, template, callback)
    apiUtils.safeAddEvent(
            event.entitySchemaLoadNamedItem,
            eventName,
            getItemName(template),
            callback)
end

--- Add a new player based on a template item.
--- @param template table|string|nil | "nil"
--- @param parameters table | "{\n    name = yourItemName\\,\n    data = {}\\,\n    components = {}\\,\n}"
function entities.addItem(template, parameters)
    apiUtils.safeCall(
        function()
            parameters.template = customEntities.template.item(getItemName(template))
            customEntities.extend(parameters)
        end)
end

--- Override components of enemies.
--- @param eventName string | "yourEventName"
--- @param callback function | "function(ev) end"
function entities.modifyAllEnemies(eventName, callback)
    apiUtils.safeAddEvent(
            event.entitySchemaLoadEnemy,
            eventName,
            {
                order = "overrides",
                sequence = 1,
            },
            callback)
end

local function getEnemyNameAndType(template)
    local enemyName = nil
    local enemyType = 1
    if template ~= nil then
        if type(template) == "table" and template.xmlName and template.xmlType then
            enemyName = template.xmlName
            enemyType = template.xmlType
        else
            debugConsole.printError("Invlalid template:")
            debugConsole.printError(template)
        end
    end

    return enemyName, enemyType
end

--- Override components of a named enemy.
--- @param eventName string | "yourEventName"
--- @param template table|nil | "namedEnemies.EnemyName"
--- @param callback function | "function(ev) end"
function entities.modifyEnemy(eventName, template, callback)
    local enemyName, enemyType = getEnemyNameAndType(template)
    apiUtils.safeAddEvent(
            event.entitySchemaLoadNamedEnemy,
            eventName,
            enemyName,
            function(ev)
                if (ev.data.type == enemyType) then
                    callback(ev)
                end
            end)
end

--- Add a new enemy based on a template enemy.
--- @param template table|nil | "namedEnemies.EnemyName"
--- @param parameters table | "{\n    name = yourEnemyName\\,\n    data = {}\\,\n    components = {}\\,\n}"
function entities.addEnemy(template, parameters)
    apiUtils.safeCall(
        function()
            parameters.template = customEntities.template.enemy(getEnemyNameAndType(template))
            customEntities.extend(parameters)
        end)
end

return entities