# Extended API for Synchrony
This mod does not add new functionality to the API, it just wrappes some methods in helper functions.

When developing, use an IDE with EmmyLua support. If you are using Visual Studio Code, add the Lua (sumneko) extension and enable 'Completion: Call Snippets (Replace)' in the settings. 

For code-completion with EmmyLua, you can then add the build-folder of the ExtendedAPI to your settings.json like this:
```
{
    "Lua.workspace.library": {
        "../SynchronyExtendedAPI/build": true,
    },
    "Lua.runtime.version": "LuaJIT",
    "editor.snippetSuggestions": "bottom",
    "Lua.diagnostics.globals": [
        "dbg",
    ],
    "Lua.workspace.useGitIgnore": true
}
```

and then add the folder to the workspace workspace.code-workspace like this:
```
{
	"folders": [
		{
			"path": ".."
		},
		{
			"path": "..\\..\\SynchronyExtendedAPI\\build\\SynchronyExtendedAPI"
		}
	],
	"settings": {
		"files.exclude": {
			"**/.git": true,
			"**/.svn": true,
			"**/.hg": true,
			"**/CVS": true,
			"**/.DS_Store": true
		}
	}
}
```

# Documentation

## Entities
```lua
local entities = require "SynchronyExtendedAPI.extended.Entities"

local namedPlayers = require "SynchronyExtendedAPI.templates.NamedPlayers"
local namedItems = require "SynchronyExtendedAPI.templates.NamedItems"
local namedEnemies = require "SynchronyExtendedAPI.templates.NamedEnemies"
```

The `Entities` class provides helper functions for adding and modifying `CustomEntities`. The ``template`` can be accessed via ``NamedPlayers``, ``NamedItems`` and ``NamedEnemies`` (eg. ``namedPlayer.Cadence``).

**Players**
- modifyAllPlayers(eventName, callback)
- modifyPlayer(eventName, template, callback)
- addPlayer(template, parameters)

**Items**
- modifyAllItems(eventName, callback)
- modifyItem(eventName, template, callback)
- addItem(template, parameters)

**Enemies**
- modifyAllEnemies(eventName, callback)
- modifyEnemy(eventName, template, callback)
- addEnemy(template, parameters)

## APIUtils
```lua
local entities = require "SynchronyExtendedAPI.utils.APIUtils"
```

The ``APIUtils`` class provides helper functions to add error-wrapping to code execution. On error, a message will be printed to the DebugConsole.

**Safety methods**
- apiUtils.safeCall(callback, parameters), eg. `safeCall(func, ev)` or `safeCall(function() callback(ev) end)`
- apiUtils.safeAddEvent(event, eventName, parameters, callback), eg. 
```lua
safeAddEvent(`
    event.objectTakeDamage,
    "myEvent",
    {
        order = "parry",
        sequence = 1,
    },
    function(ev)
        -- do something
    end
```
- apiUtils.safeOverrideEvent(event, eventName, parameters, callback), eg.
```lua
safeOverrideEvent(`
    event.objectTakeDamage,
    "myEvent",
    {
        order = "parry",
        sequence = 1,
    },
    function(func, ev)
        -- do something
    end
```

## DebugConsole
```lua
local debugConsole = require "SynchronyExtendedAPI.utils.DebugConsole"
```

Provides methods to print debug or error messages to the screen, without interrupting the game. Debug messages will only be displayed in singleplayer.
Messages will be line-wrapped to 60 characters, and a maximum of 20 lines can be displayed at once. All messages are logged to the `Synchrony.log` as well.

**Methods**
- printError(message) - prints red message
- printDebug(message) - prints blue message
- print(message, color) - prints message in a custom color
```lua
local color = require "system.utils.Color"
debugConsole.print("message", color.rgba(255, 255, 255, 255))
```

## ItemPool and EnemyPool
```lua
local itemPool = require "SynchronyExtendedAPI.pools.ItemPool"
local enemyPool = require "SynchronyExtendedAPI.pools.EnemyPool"
```

Provides methods for adding weighted replacement chances for placed entities on level transition. Mainly intended to be used with [ExtendedAPI - Pools (WIP)](https://gitlab.com/romst/synchronyextendedapipools).

The names are the Synchrony names (see also: `NamedEnemies.name`, `NamedItems.name`). If `overwriteBaseType == true`, the original type won't be added to the pool. Otherwise it will be added with a weight of 50.

**EnemyPool**
- addReplacement(enemyName, replacementName, weight, overwriteBaseType)

**ItemPool**
- addShopReplacement(shopkeeperName, itemName, replacementName, price, weight, overwriteBaseType)
- addHolderReplacement(holderName, itemName, replacementName, weight, overwriteBaseType)
- addFloorReplacement(itemName, replacementName, weight, overwriteBaseType)